from django.db import models

# Create your models here.
class Note(models.Model):
    sender = models.TextField()
    receiver = models.TextField()
    title = models.TextField()
    message = models.TextField()