from django.http import response
from django.shortcuts import redirect, render
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')

# Create your views here.
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response) 

@login_required(login_url='/admin/login/')

def add_friend(request):
    add_friend = FriendForm(request.POST or None)
    if (add_friend.is_valid() and request.method == 'POST'):
        add_friend.save()
        return redirect('/lab-3')
    response = {'add_friend':add_friend}
    return render(request, 'lab3_form.html', response)