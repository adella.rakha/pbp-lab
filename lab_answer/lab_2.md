# **Lab 2 Answers**
## **1. HTML VS XML**
Berdasarkan kepanjangannya sendiri sudah berbeda dimana XML adalah eXtensible Markup Language sedangkan HTML adalah Hypertext Markup Language. Purpose dari HTML sendiri lebih ke tampilan data sedangkan XML untuk menyimpan dan mentransfer data.
XML juga lebih case sensitive dibandingkan HTML, dimana di XML < form > berbeda dengan < Form >. Closing Tag XML juga lebih strict dibandingkan HTML.
## **2. HTML VS JSON**
Berdasarkan kepanjangannya sendiri sudah berbeda dimana JSON adalah JavaScript Object Notation sedangkan HTML adalah Hypertext Markup Language. Konsepnya mirip seperti XML, JSON juga fokus dalam menyimpan dan mentransfer data. Yang membedakannya adalah JSON menggunakan JavaScript language (key dan value).
Di dalam JSON, dia tidak menggunakan tag seperti HTML. Tampilannya jadi seperti array.

### Referensi : https://www.geeksforgeeks.org/difference-between-json-and-xml/