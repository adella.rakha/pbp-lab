from django.http import response
from django.shortcuts import redirect, render
from lab_2.models import Note
from lab_4.forms import NoteForm

# Create your views here.
def index(request):
    notes = Note.objects.all().values()  # TODO Implement this
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)
