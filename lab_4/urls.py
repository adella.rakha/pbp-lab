from django.urls import path
from .views import index, add_note, note_list

urlpatterns = [
    path('', index, name='index'),
    # TODO Add friends path using friend_list Views
    path('add-note', add_note,name="Add Note"),
    path('note-list', note_list,name="Note Cards"),
]