from django.http import response
from django.shortcuts import redirect, render
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    notes = Note.objects.all().values()  # TODO Implement this
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    add_note = NoteForm(request.POST or None)
    if (add_note.is_valid() and request.method == 'POST'):
        add_note.save()
        return redirect('/lab-4')
    response = {'add_note':add_note}
    return render(request, 'lab4_form.html', response)

def note_list(request):
    notes = Note.objects.all().values()  
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)